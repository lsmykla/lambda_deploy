#!/bin/bash

# Prevent making more than 1 deploy per second
sleep 1

if [[ "$#" -ne 1 ]]; then
  tput setaf 3; echo "Please enter Lambda function name"; tput sgr 0
  exit 1;
fi

# Lambda function name
LAMBDA=${1}

which aws 1>/dev/null

if [[ $? -ne 0 ]]; then
    tput setaf 1; echo "Aws cli tool seems to be not installed"; tput sgr 0
    echo
    tput setaf 4
    echo "Simple instruction:"
    echo "1. Install aws cli tool (see: https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html)"
    echo "2. Create secure user (see: https://docs.aws.amazon.com/general/latest/gr/root-vs-iam.html)"
    echo "3. Configure your aws cli tool (see: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)"
    echo "4. Back here :)"
    tput sgr 0

    exit 1
fi

# Check if given function exists
aws lambda get-function --function-name ${LAMBDA} &>/dev/null

if [[ ${?} = 255 ]]; then
    tput setaf 1; echo ${LAMBDA}" function not found or permission denied"; tput sgr 0
    exit 1
fi

# File name base on current timestamp + date as a 'YY_MM_DD' format + index wix zip extension
FILE_NAME=$(date +%s_%y_%m_%d)"_index.zip"

# Declare ignored files
IGNORE_FILES=""

# Get previous zip files as a beginning of ignored list
if [[ -r ".history" ]]; then
    IGNORE_FILES_LIST=$(cat ".history" | tr '\n' ' ')

    SIZE=${#IGNORE_FILES_LIST}

    if [[ ${SIZE} -gt 0 ]]
    then
        IGNORE_FILES=${IGNORE_FILES_LIST}
    fi
fi

# Get next ignored files
if [[ -r ".ignore" ]]; then
    IGNORE_FILES_LIST=$(cat ".ignore" | tr '\n' ' ')

    SIZE=${#IGNORE_FILES_LIST}

    if [[ ${SIZE} -gt 0 ]]
    then
        IGNORE_FILES=${IGNORE_FILES}" "${IGNORE_FILES_LIST}
    fi
fi

SIZE=${#IGNORE_FILES}

if [[ ${SIZE} -gt 0 ]]
then
    IGNORE_FILES="-x "${IGNORE_FILES}
fi

# Make zip file
function zipFile() {
    tput setaf 4
    echo "Packing files: "
    tput setaf 6
    zip -r ${FILE_NAME} * ${IGNORE_FILES}
    tput setaf 4
    echo "_____________________________________________________"
    tput sgr 0
}

(
    set -e
    zipFile
)

if [[ $? -gt 0 ]]
then
  tput setaf 1; echo "Something went wrong during making zip file"; tput sgr 0
  exit 1
fi

###################### U P L O A D  Z I P  F I L E ######################

function uploadFile() {
    echo ''
    aws lambda update-function-code --function-name ${LAMBDA} --zip-file fileb://${FILE_NAME} &>/dev/null
}

(
    set -e
    uploadFile
    tput bold
    tput setaf 2
    echo "!!! "${FILE_NAME}" UPLOADED SUCCESSFULLY !!!"
    tput sgr 0
)

if [[ $? -gt 0 ]]
then
  tput setaf 1; echo "Something went wrong during uploading zip file"; tput sgr 0
  exit 1
fi

#########################################################################

# Update Registry
function updateHistory() {

    if [[ -w ".history" ]]; then
        echo ${FILE_NAME} | tee -a .history 1>/dev/null
        else
        touch ".history"
        echo ${FILE_NAME} | tee -a .history 1>/dev/null
    fi
}

(
    set -e
    updateHistory
)

if [[ $? -gt 0 ]]
then
    tput setaf 1; echo "Something went wrong during history file updating"; tput sgr 0
    exit 1
fi

# Remove uploaded file
tput setaf 4;
read -p "Do you want to remove zip file after successful deploy? [y/n]" -n 1 -r
tput sgr 0

echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
   rm -f ${FILE_NAME}
fi
