# Lambda Deploy

Lambda Deploy is a simple sh script for deploying your application to AWS Lambda. It create zip file with all 
application files, except ignore list, and upload to Lambda instance.  

Configuration
-------------
* Copy files from list below to you root catalog:
    - deploy.sh
    - .ignore
    - .history
* Install aws cli [tool][1]
* Create secure [user][2]
* [Configure][3] your aws cli tool
* Prepare list with ignored files (.ignore)

Usage
-----
Simply run ```./deploy.sh <lambda_function_name>``` and check notifications. After all you should decide delete zip file 
or not. 

#####  Possible exceptions

- Lambda function name required
- Aws cli tool not installed
- Wrong function name or permission denied
- Zip file making exception
- Zip file uploading exception
- History file updating exception

Files
-----
* deploy.sh - main script with all logic
* .ignore - list with all ignored files
* .history - simple list with all zip files already created and successfully uploaded


[1]: https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html
[2]: https://docs.aws.amazon.com/general/latest/gr/root-vs-iam.html
[3]: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html